from __future__ import division
from psychopy import visual, event, core, logging, sound
import os
import joblib
import numpy as np
import yaml
import sys

def get_indices(file_name, n_lim=50):
    probabilities = joblib.load(file_name)
    indices_to_use = np.unique(np.concatenate(
        [np.argsort(probabilities[key])[-n_lim:] for key in probabilities.keys()]))
    return indices_to_use

vp = sys.argv[1]

file_name = 'classification_FG_ridge_logBSC_H200_predictions.pkl'
indices_to_use = get_indices(file_name, n_lim=50)
#NEW INDICES HERE
indices_2 = joblib.load('examples_2.pkl')
indices_to_use = np.unique(np.concatenate([indices_to_use, indices_2]))
print(indices_to_use.shape)

sample_list = [str(idx) + '.wav' for idx in indices_to_use]
np.random.shuffle(sample_list)
# create a window before creating your rating scale, whatever units you like:
win = visual.Window(fullscr=False, size=[1100, 800], units='pix', monitor='testMonitor')

instr = visual.TextStim(win, text="""Hi! Your task is to rate a set of stimuli on their noisiness -- how loud you hear noise in contrast to speech. 
First, you will listen to a stimulus, where very little noise is present.
Press any button to continue.
        """)
event.clearEvents()
instr.draw()
win.flip()
if 'escape' in event.waitKeys():
    core.quit()

win.flip()
core.wait(0.35)
myItem = sound.Sound('sn15_trim.wav')
myItem.play()
core.wait(6)

instr = visual.TextStim(win, text="""Now you will hear the same audio sample corrupted by a large amount of noise. 
Afterwards, you will see a rating scale of the noise level. Please choose your perceived level of noisiness. 
Press any button to continue.""")
event.clearEvents()
instr.draw()
win.flip()
if 'escape' in event.waitKeys():
    core.quit()

win.flip()
core.wait(0.35)
myItem = sound.Sound('sn0_trim.wav')
myItem.play()
core.wait(6)

myRatingScale = visual.RatingScale(win, low=0, high=6, precision=1, scale='Noise level',
                                   labels=['No noise', 'noisy', 'very noisy'], name='Noisiness')
while myRatingScale.noResponse:
    myRatingScale.draw()
    win.flip()
    if event.getKeys(['escape']):
        core.quit()

instr = visual.TextStim(win, text="""The first example had very little noise present, the second example a large amount.
In the following experiment you will hear different audio samples. Some change their level of noise throughout the audio sample. Please indicate your perceived noisiness over the whole sample in your answer. Additionally, the type of noise differs between samples.

The experiment will take around 25 minutes.
Press any button to continue.
        """)
event.clearEvents()
instr.draw()
win.flip()
if 'escape' in event.waitKeys():
    core.quit()

myRatingScale.reset()  # reset between repeated uses of the same scale
event.clearEvents()

win.flip()
core.wait(0.35)

data = {}
for n, speech in enumerate(sample_list):
    counter = visual.TextStim(win, text='{}/{} samples'.format(n+1,len(sample_list)), alignHoriz='right', alignVert='top')
    counter.draw()
    x, y = myRatingScale.win.size
    myItem = sound.Sound('/home/mboos/Work/sample_wavs/' + speech)
    # rate each speech on two dimensions
    myRatingScale.reset()  # reset between repeated uses of the same scale
    event.clearEvents()
#    core.wait(0.5)  # brief pause, slightly smoother for the subject
    myItem.play()
    core.wait(6)
    while myRatingScale.noResponse:
        myRatingScale.draw()
        win.flip()
        if event.getKeys(['escape']):
            core.quit()
    data[speech] = (myRatingScale.getRating(), myRatingScale.getRT(), n)  # save for later

    # clear the screen & pause between ratings
    win.flip()
    core.wait(0.35)  # brief pause, slightly smoother for the subject

with open('{}.yml'.format(vp), 'w+') as f:
    yaml.dump(data, f)

win.close()
core.quit()
